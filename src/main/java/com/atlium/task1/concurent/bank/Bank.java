package com.atlium.task1.concurent.bank;

import com.atlium.task1.exception.NotEnoughMoneyException;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bank {
    private int moneyAmount;
    private String name;
    private int bankruptThreshold;
    private final Lock bankLock;
    private final Condition enoughMoneyCondition;


    public Bank(int moneyAmount, String name, int bankruptThreshold) {
        this.moneyAmount = moneyAmount;
        this.name = name;
        this.bankruptThreshold = bankruptThreshold;
        bankLock = new ReentrantLock();
        enoughMoneyCondition = bankLock.newCondition();
    }

    public void transferMoney(int amount) throws NotEnoughMoneyException, InterruptedException {
        System.out.println(String.format("Thread name:%s (in) to (transferMoney)", Thread.currentThread().getName()));
        try {
            bankLock.lock();
            while (!hasMoney(amount)) {
                enoughMoneyCondition.await();
            }
            System.out.println(String.format("transferMoney:params:bankAmount:%d, needAmount:%d", moneyAmount, amount));
            if (amount > moneyAmount) {
                throw new NotEnoughMoneyException(moneyAmount, amount);
            }
            moneyAmount -= amount;
            System.out.println(String.format("Thread name:%s (out) from (transferMoney) bankMoneyAmount:%s", Thread.currentThread().getName(), moneyAmount));
            enoughMoneyCondition.signalAll();
        } finally {
            bankLock.unlock();
        }
    }

    public void putMoney(int amount) {
        System.out.println(String.format("Thread name:%s (in) to (putMoney)", Thread.currentThread().getName()));
        try {
            bankLock.lock();
            System.out.println(String.format("putMoney:params:bankAmount:%d, putAmount:%d", moneyAmount, amount));
            moneyAmount += amount;
            System.out.println(String.format("Thread name:%s (out) from (putMoney) bankMoneyAmount:%s", Thread.currentThread().getName(), moneyAmount));
            enoughMoneyCondition.signalAll();
        } finally {
            bankLock.unlock();
        }
    }

    public boolean hasMoney(int amount) {
        System.out.println(String.format("Thread name:%s (in) to (hasMoney)", Thread.currentThread().getName()));
        System.out.println(String.format("hasMoney:params:bankAmount:%d, needAmount:%d", moneyAmount, amount));
        boolean result = moneyAmount > amount;
        System.out.println(String.format("Thread name:%s (out) from (hasMoney) with result:%s", Thread.currentThread().getName(), result));
        return result;
    }

    public int getMoneyAmount() {
        return moneyAmount;
    }

    public String getName() {
        return name;
    }

    public boolean isBankrupt() {
        try {
            bankLock.lock();
            return moneyAmount < bankruptThreshold;
        } finally {
            bankLock.unlock();
        }
    }
}
