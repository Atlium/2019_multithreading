package com.atlium.task1.sync;

import com.atlium.task1.sync.bank.Bank;
import com.atlium.task1.sync.bank.BankInvestor;
import com.atlium.task1.sync.bank.BankUser;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static final int BANK_INITIAL_AMOUNT = 1_000_000;
    private static final int INVESTOR_INITIAL_AMOUNT = 10_000;
    private static final int USER_LIMIT = 20;
    private static final int INVESTOR_LIMIT = 2;
    private static final int BANKRUPT_THRESHOLD = 100;

    public static void main(String[] args) {
        System.out.println("MAIN WITH SYNC");
        Bank bank = new Bank(BANK_INITIAL_AMOUNT, "SberBank", BANKRUPT_THRESHOLD);
        List<Thread> users = Stream.iterate(0, i -> i + 1)
                .limit(USER_LIMIT)
                .map(index -> new Thread(new BankUser(bank, BANKRUPT_THRESHOLD * index + 10)))
                .collect(Collectors.toList());
        List<BankInvestor> investors = Stream.iterate(0, i -> i + 1)
                .limit(INVESTOR_LIMIT)
                .map(index -> new BankInvestor(bank, BANKRUPT_THRESHOLD + BANKRUPT_THRESHOLD * index, INVESTOR_INITIAL_AMOUNT))
                .collect(Collectors.toList());
        users.forEach(Thread::start);
        investors.forEach(investor -> new Thread(investor).start());
        try {
            while (investors.stream().allMatch(BankInvestor::hasAvailableMoney) && !bank.isBankrupt()) {
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        } finally {
            users.forEach(Thread::interrupt);
        }
        System.out.println("bank money left:" + bank.getMoneyAmount());
    }
}
