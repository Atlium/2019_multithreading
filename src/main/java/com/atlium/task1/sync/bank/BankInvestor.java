package com.atlium.task1.sync.bank;

public class BankInvestor implements Runnable {
    private Bank bank;
    private int charityAmount;
    private int capital;

    public BankInvestor(Bank bank, int charityAmount, int capital) {
        this.bank = bank;
        this.charityAmount = charityAmount;
        this.capital = capital;
    }

    @Override
    public void run() {
        while (hasAvailableMoney()) {
            bank.putMoney(getAvailableMoney());
        }
        System.out.println(String.format("Investor %s finish his investing", Thread.currentThread().getName()));
    }

    public boolean hasAvailableMoney() {
        return capital > 0;
    }

    private int getAvailableMoney() {
        if (charityAmount < capital) {
            capital -= charityAmount;
            return charityAmount;
        }
        int availableMoney = capital;
        capital = 0;
        return availableMoney;
    }
}
