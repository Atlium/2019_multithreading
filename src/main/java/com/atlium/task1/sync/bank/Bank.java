package com.atlium.task1.sync.bank;

import com.atlium.task1.exception.NotEnoughMoneyException;

public class Bank {
    private int moneyAmount;
    private String name;
    private int bankruptThreshold;

    public Bank(int moneyAmount, String name, int bankruptThreshold) {
        this.moneyAmount = moneyAmount;
        this.name = name;
        this.bankruptThreshold = bankruptThreshold;
    }

    public void transferMoney(int amount) throws NotEnoughMoneyException, InterruptedException {
        System.out.println(String.format("Thread name:%s (in) to (transferMoney)", Thread.currentThread().getName()));
        synchronized (this) {
            while (!hasMoney(amount)) {
                wait();
            }
            System.out.println(String.format("transferMoney:params:bankAmount:%d, needAmount:%d", moneyAmount, amount));
            if (amount > moneyAmount) {
                throw new NotEnoughMoneyException(moneyAmount, amount);
            }
            moneyAmount -= amount;
            System.out.println(String.format("Thread name:%s (out) from (transferMoney) bankMoneyAmount:%s", Thread.currentThread().getName(), moneyAmount));
            notifyAll();
        }
    }

    public void putMoney(int amount) {
        System.out.println(String.format("Thread name:%s (in) to (putMoney)", Thread.currentThread().getName()));
        synchronized (this) {
            System.out.println(String.format("putMoney:params:bankAmount:%d, putAmount:%d", moneyAmount, amount));
            moneyAmount += amount;
            System.out.println(String.format("Thread name:%s (out) from (putMoney) bankMoneyAmount:%s", Thread.currentThread().getName(), moneyAmount));
            notifyAll();
        }
    }

    public boolean hasMoney(int amount) {
        System.out.println(String.format("Thread name:%s (in) to (hasMoney)", Thread.currentThread().getName()));
        System.out.println(String.format("hasMoney:params:bankAmount:%d, needAmount:%d", moneyAmount, amount));
        boolean result = moneyAmount > amount;
        System.out.println(String.format("Thread name:%s (out) from (hasMoney) with result:%s", Thread.currentThread().getName(), result));
        return result;
    }

    public int getMoneyAmount() {
        return moneyAmount;
    }

    public String getName() {
        return name;
    }

    public synchronized boolean isBankrupt() {
        return moneyAmount < bankruptThreshold;
    }
}
