package com.atlium.task1.nosync;

import com.atlium.task1.nosync.bank.Bank;
import com.atlium.task1.nosync.bank.BankUser;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static final int INITIAL_AMOUNT = 1_000_000;
    private static final int USER_LIMIT = 20;

    public static void main(String[] args) {
        System.out.println("MAIN WITHOUT SYNC");
        Bank bank = new Bank(INITIAL_AMOUNT, "SberBank");
        List<Thread> users = Stream.iterate(0, i -> i + 1)
                .limit(USER_LIMIT)
                .map(index -> new Thread(new BankUser(bank, 100 * index + 10)))
                .collect(Collectors.toList());
        users.forEach(Thread::start);
    }
}
