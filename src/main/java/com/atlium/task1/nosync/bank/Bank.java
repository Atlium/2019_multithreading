package com.atlium.task1.nosync.bank;

import com.atlium.task1.exception.NotEnoughMoneyException;

public class Bank {
    private int moneyAmount;
    private String name;

    public Bank(int moneyAmount, String name) {
        this.moneyAmount = moneyAmount;
        this.name = name;
    }

    public void transferMoney(int amount) throws NotEnoughMoneyException {
        System.out.println(String.format("Thread name:%s (in) to (transferMoney)", Thread.currentThread().getName()));
        System.out.println(String.format("transferMoney:params:bankAmount:%d, needAmount:%d", moneyAmount, amount));
        if (amount > moneyAmount) {
            throw new NotEnoughMoneyException(moneyAmount, amount);
        }
        moneyAmount -= amount;
        System.out.println(String.format("Thread name:%s (out) from (transferMoney) bankMoneyAmount:%s", Thread.currentThread().getName(), moneyAmount));
    }

    public boolean hasMoney(int amount) {
        System.out.println(String.format("Thread name:%s (in) to (hasMoney)", Thread.currentThread().getName()));
        System.out.println(String.format("hasMoney:params:bankAmount:%d, needAmount:%d", moneyAmount, amount));
        boolean result = moneyAmount > amount;
        System.out.println(String.format("Thread name:%s (out) from (hasMoney) with result:%s", Thread.currentThread().getName(), result));
        return result;
    }

    public int getMoneyAmount() {
        return moneyAmount;
    }

    public String getName() {
        return name;
    }
}
