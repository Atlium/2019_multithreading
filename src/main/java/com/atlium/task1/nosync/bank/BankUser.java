package com.atlium.task1.nosync.bank;

import com.atlium.task1.exception.NotEnoughMoneyException;

public class BankUser implements Runnable {

    private Bank bank;
    private int withdrawAmount;

    public BankUser(Bank bank, int withdrawAmount) {
        this.bank = bank;
        this.withdrawAmount = withdrawAmount;
    }

    @Override
    public void run() {
        try {
            while (bank.hasMoney(withdrawAmount)) {
                bank.transferMoney(withdrawAmount);
            }
        } catch (NotEnoughMoneyException e) {
            System.out.println(String.format("-ERROR###thread:%s handle exception:%s", Thread.currentThread().getName(), e.getMessage()));
        }
    }
}
