package com.atlium.task1.exception;

public class NotEnoughMoneyException extends Exception {
    private final int amount;
    private final int needAmount;

    public NotEnoughMoneyException(int amount, int needAmount) {
        this.amount = amount;
        this.needAmount = needAmount;
    }

    @Override
    public String getMessage() {
        return String.format("Not enough money, need:%d, have:%d", needAmount, amount);
    }
}
