package com.atlium.task3.brute;

public enum Status {
    SUCCESS, FAILURE, IN_PROGRESS
}
