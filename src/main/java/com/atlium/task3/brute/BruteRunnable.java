package com.atlium.task3.brute;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class BruteRunnable implements Runnable {
    private final String passHash;
    private final char[] allowedSymbols;
    private final char[] firstSymbols;
    private final BruteDispatcher dispatcher;
    private int maxPassLength;
    private boolean isActive = true;

    private int counter;
    private String lastPass;

    public BruteRunnable(String passHash, char[] allowedSymbols, char[] firstSymbols, BruteDispatcher dispatcher, int maxPassLength) {
        this.passHash = passHash;
        this.allowedSymbols = allowedSymbols;
        this.firstSymbols = firstSymbols;
        this.dispatcher = dispatcher;
        this.maxPassLength = maxPassLength;
    }

    @Override
    public void run() {
        int passLength = 2;
        ResultHolder resultHolder = new ResultHolder();
        while (isActive && resultHolder.getStatus() != Status.SUCCESS && passLength <= maxPassLength) {
            resultHolder = brutePass(allowedSymbols, firstSymbols, passLength++, passHash);
        }
        if (resultHolder.getStatus() == Status.SUCCESS) {
            dispatcher.success(resultHolder);
        }
        System.out.println(toString());
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    private ResultHolder brutePass(char[] allowedSymbols, char[] firstSymbols, int passLength, String passHash) {
        final int maxFirstIndex = firstSymbols.length - 1;
        final int maxIndex = allowedSymbols.length - 1;
        ResultHolder resultHolder = new ResultHolder();
        int lastIndex = 0;
        int[] indexes = new int[passLength];
        char[] worker = new char[passLength];
        boolean needContinue = true;
        int to = passLength;
        while (isActive && needContinue) {
            worker[0] = firstSymbols[indexes[0]];
            for (int i = 1; i < to; i++) {
                worker[i] = allowedSymbols[indexes[i]];
            }
            counter++;
            lastPass = new String(worker);
            System.out.println(Thread.currentThread().getName() + " : " + counter);
//            System.out.println(Thread.currentThread().getName() + " : " + lastPass + " : " + calcHash(lastPass));

            if (passHash.equals(calcHash(lastPass))) {
                needContinue = false;
                resultHolder.update(new ResultHolder(Status.SUCCESS, new String(worker)));
                continue;
            }

            while (indexes[lastIndex] == (lastIndex == 0 ? maxFirstIndex : maxIndex) && lastIndex != indexes.length - 1) {
                lastIndex++;
            }

            to = lastIndex + 1;

            if (lastIndex == indexes.length - 1 && indexes[lastIndex] == maxIndex) {
                needContinue = false;
                continue;
            }

            indexes[lastIndex] += 1;
            for (int i = 0; i < lastIndex; i++) {
                indexes[i] = 0;
            }
            lastIndex = 0;
        }
        return resultHolder;
    }

    public String calcHash(String toHash) {
        Hasher hasher = Hashing.md5().newHasher();
        hasher.putString(toHash, StandardCharsets.UTF_8);
        return hasher.hash().toString();
    }

    @Override
    public String toString() {
        return "BruteCallable{" +
                "performedCount=" + counter +
                ", lastPerformed='" + lastPass + '\'' +
                '}';
    }
}
