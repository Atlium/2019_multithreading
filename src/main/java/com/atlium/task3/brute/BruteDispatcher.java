package com.atlium.task3.brute;

import java.util.List;

public class BruteDispatcher {
    private List<BruteRunnable> runnableList;

    public BruteDispatcher() {
    }

    public List<BruteRunnable> getThreads() {
        return runnableList;
    }

    public void setThreads(List<BruteRunnable> runnableList) {
        this.runnableList = runnableList;
    }

    public synchronized void success(ResultHolder resultHolder) {
        System.out.println("PASS:" + resultHolder.getValue());
        for (BruteRunnable runnable : runnableList) {
            runnable.setActive(false);
        }
    }
}
