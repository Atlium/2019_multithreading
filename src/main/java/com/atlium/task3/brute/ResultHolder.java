package com.atlium.task3.brute;

public class ResultHolder {
    private Status status;
    private String value;

    public ResultHolder() {
        status = Status.IN_PROGRESS;
    }

    public ResultHolder(Status status, String value) {
        this.status = status;
        this.value = value;
    }

    public Status getStatus() {
        return status;
    }

    public String getValue() {
        return value;
    }

    public void update(ResultHolder resultHolder) {
        this.status = resultHolder.status;
        this.value = resultHolder.value;
    }

    @Override
    public String toString() {
        return "ResultHolder{" +
                "status=" + status +
                ", value='" + value + '\'' +
                '}';
    }
}
