package com.atlium.task3;

import com.atlium.task3.brute.BruteDispatcher;
import com.atlium.task3.brute.BruteRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    private static final int PASS_MAXIMUM_LENGTH = 8;
    private static final int THREAD_COUNT = 2;
    private static final String PASS_HASH = "DDC4035FF6451F85BB796879E9E5EA49".toLowerCase();
    private static final String ALLOWED_CHARS = "abcdefghijklmnopqrstuvwxyz";
    private static final String TEST_HASH = "ab56b4d92b40713acc5af89985d4b786"; //pass abcde

    public static List<char[]> splitEqually(char[] chars, int size) {
        List<StringBuilder> characters = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            characters.add(new StringBuilder());
        }
        int minus = 0;
        for (int i = 0; i < chars.length; i++) {
            if (i - minus >= size) {
                minus += size;
            }
            characters.get(i - minus).append(chars[i]);
        }
        return characters
                .stream()
                .map(StringBuilder::toString)
                .map(String::toCharArray)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<char[]> chars = splitEqually(ALLOWED_CHARS.toCharArray(), THREAD_COUNT);
        BruteDispatcher bruteDispatcher = new BruteDispatcher();
        List<BruteRunnable> runnableList = new ArrayList<>();
        for (int i = 0; i < THREAD_COUNT; i++) {
            runnableList.add(new BruteRunnable(PASS_HASH, ALLOWED_CHARS.toCharArray(), chars.get(i), bruteDispatcher, PASS_MAXIMUM_LENGTH));
//            runnableList.add(new BruteRunnable(TEST_HASH, ALLOWED_CHARS.toCharArray(), chars.get(i), bruteDispatcher, 5));
        }
        List<Thread> threads = runnableList.stream().map(Thread::new).collect(Collectors.toList());
        bruteDispatcher.setThreads(runnableList);
        threads.forEach(Thread::start);
    }
}
